﻿using System;
using System.Collections.Generic;

namespace demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in your name and press <ENTER>");
            var name = Console.ReadLine();

            //Console.WriteLine("Please type in your age and press <ENTER>");
            //var age = Console.ReadLine();

            //Console.WriteLine("Hi my name is "+ name + " and I am " + age + " years old");
            //Console.WriteLine("Hi my name is {0} and I am {1} years old", name, age);
            //Console.WriteLine($"HI my name is {name} and I am {age} years old"); //String Interpolation

            ////If Statement Structure
            //if (name == "Scotty")
            //{
            //    Console.WriteLine("You guessed my name!");
            //}
            //else
            //{
            //    Console.WriteLine("You where wrong!");
            //}

            //ARRAYS
            //var fruits = new string[3] { "tomato", "oranges", "peaches" };

            //String Join - adds everything on a single line
            //Console.WriteLine(string.Join(", ", fruits));

            //LOOPS
            //for(var i = 0; i < fruits.Length; i++)
            //{
            //    //e.g. fruits[2];
            //    Console.WriteLine(fruits[i]);
            //}

            //foreach(var x in fruits)
            //{
            //    Console.WriteLine(x);
            //}

            //Console.WriteLine($"Please select a fruit that you like by typing a number");

            ////LOOPS
            //for (var i = 0; i < fruits.Length; i++)
            //{
            //    //e.g. fruits[2];
            //    Console.WriteLine($"{i} - {fruits[i]}");
            //}

            //var selection = int.Parse(Console.ReadLine());

            //Console.WriteLine($"Hi I am {name} and I like {fruits[selection]}");
      

            Console.ReadLine();
        }
    }
}